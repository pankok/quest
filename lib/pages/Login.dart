import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool selected = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: AnimatedContainer(
          alignment: Alignment.center,
          duration: Duration(milliseconds: 600),
          decoration: BoxDecoration(
              color: selected? Color(0xffF6404F):Colors.white,
              image: DecorationImage(
                  image: AssetImage("images/back.png"), fit: BoxFit.cover)),
          padding: EdgeInsets.only(left: 30, right: 30),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 12),
                    child: Image.asset(
                      "images/logo.png",
                      width: 200,
                      height: 150,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 12),
                    child: TextField(
                      // controller: _controllerEmail,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(2, 16, 2, 16),
                        hintText: "Email",
                        filled: false,
                      ),
                    ),
                  ),
                  !selected ? Padding(
                    padding: EdgeInsets.only(bottom: 12),
                    child: TextField(
                      // controller: _controllerEmail,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(2, 16, 2, 16),
                        hintText: "Nome",
                        filled: false,
                      ),
                    ),
                  ):Container(),
                  Padding(
                    padding: EdgeInsets.only(bottom: 12),
                    child: TextField(
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.black),
                      decoration: InputDecoration(
                        // contentPadding: EdgeInsets.fromLTRB(2, 16, 2, 16),
                        hintText: "Senha",
                        filled: false,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(36, 40, 36, 10),
                    child: RaisedButton(
                        child: Text(selected?
                          "Login":"Cadastrar-se",
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        ),
                        color: selected?Color(0xff30BCC9):Color(0xffF6404F),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(48.0),
                          side: BorderSide(color: Colors.black, width: 0.1),
                        ),
                        padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
                        onPressed: () {
                          // _validarCampos();
                        }),
                  ),
                  Center(
                    child: GestureDetector(
                      child: Text(
                        "Não tem conta? cadastre-se!",
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                      onTap: () {
                        setState(() {
                          selected = !selected;
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
