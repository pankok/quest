import 'package:flutter/material.dart';
import 'package:project_quest/pages/Login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.black,
        primarySwatch: Colors.teal,
      ),
      home:Login() 
      // Center(child: Image.asset("images/logo.png"),),
    );
  }
}



